from flask import Flask, render_template
from flask import request
import json
import os
import sys
sys.path.insert(0, '/home/ankesh/traderbot/')
from client import get_success_record

app = Flask(__name__)


@app.route('/1m')
def _1m():
	listed = [
	    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
	    'latest price of (1)', 'percentage change in price (1)',
	    'moving average of volume (2)', 'latest volume (2)',
	    'percentage change in volume (2)',
	    'percentage change in volume wrt 24h volume of (2)',
	    'Moving average(7) > Moving average(25)'
	]
	with open('/home/ankesh/traderbot/temp.json', 'r') as f:
		all_values = json.load(f)
	try:
		# get_success_record(all_values['1m'])
		return render_template('result.html', result=all_values['1m'], listed=listed)
	except KeyError as e:
		return json.dumps("Data not found")


@app.route('/5m')
def _5m():
	listed = [
	    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
	    'latest price of (1)', 'percentage change in price (1)',
	    'moving average of volume (2)', 'latest volume (2)',
	    'percentage change in volume (2)',
	    'percentage change in volume wrt 24h volume of (2)',
	    'Moving average(7) > Moving average(25)'
	]
	with open('/home/ankesh/traderbot/temp.json', 'r') as f:
		all_values = json.load(f)
	try:
		# get_success_record(all_values['5m'])
		return render_template('result.html', result=all_values['5m'], listed=listed)
	except KeyError as e:
		return json.dumps("Data not found")


@app.route('/15m')
def _15m():
	listed = [
	    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
	    'latest price of (1)', 'percentage change in price (1)',
	    'moving average of volume (2)', 'latest volume (2)',
	    'percentage change in volume (2)',
	    'percentage change in volume wrt 24h volume of (2)',
	    'Moving average(7) > Moving average(25)'
	]
	with open('/home/ankesh/traderbot/temp.json', 'r') as f:
		all_values = json.load(f)
	try:
		# get_success_record(all_values['15m'])
		return render_template(
		    'result.html', result=all_values['15m'], listed=listed)
	except KeyError as e:
		return json.dumps("Data not found")


@app.route('/30m')
def _30m():
	listed = [
	    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
	    'latest price of (1)', 'percentage change in price (1)',
	    'moving average of volume (2)', 'latest volume (2)',
	    'percentage change in volume (2)',
	    'percentage change in volume wrt 24h volume of (2)',
	    'Moving average(7) > Moving average(25)'
	]
	with open('/home/ankesh/traderbot/temp.json', 'r') as f:
		all_values = json.load(f)
	try:
		return render_template(
		    'result.html', result=all_values['30m'], listed=listed)
	except KeyError as e:
		return json.dumps("Data not found")


@app.route('/1h')
def _1h():
	listed = [
	    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
	    'latest price of (1)', 'percentage change in price (1)',
	    'moving average of volume (2)', 'latest volume (2)',
	    'percentage change in volume (2)',
	    'percentage change in volume wrt 24h volume of (2)',
	    'Moving average(7) > Moving average(25)'
	]
	with open('/home/ankesh/traderbot/temp.json', 'r') as f:
		all_values = json.load(f)
	try:
		return render_template('result.html', result=all_values['1h'], listed=listed)
	except KeyError as e:
		return json.dumps("Data not found")


@app.route('/4h')
def _4h():
	listed = [
	    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
	    'latest price of (1)', 'percentage change in price (1)',
	    'moving average of volume (2)', 'latest volume (2)',
	    'percentage change in volume (2)',
	    'percentage change in volume wrt 24h volume of (2)',
	    'Moving average(7) > Moving average(25)'
	]
	with open('/home/ankesh/traderbot/temp.json', 'r') as f:
		all_values = json.load(f)
	try:
		return render_template('result.html', result=all_values['4h'], listed=listed)
	except KeyError as e:
		return json.dumps("Data not found")


@app.route('/result')
def result():
	listed = [
	    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
	    'latest price of (1)', 'percentage change in price (1)',
	    'moving average of volume (2)', 'latest volume (2)',
	    'percentage change in volume (2)',
	    'percentage change in volume wrt 24h volume of (2)',
	    'Moving average(7) > Moving average(25)'
	]
	with open('/home/ankesh/traderbot/temp.json', 'r') as f:
		all_values = json.load(f)
	try:
		return render_template('result.html', result=all_values['1m'], listed=listed)
	except KeyError as e:
		return json.dumps("Data not found")


if __name__ == '__main__':
	app.run(debug=True)