import sys
from multiprocessing.connection import Listener
from binance.client import Client
from binance.websockets import BinanceSocketManager
from binance.enums import *
from binance.exceptions import BinanceAPIException
import os
import csv
import env
import signal
import datetime
import json
from collections import defaultdict
import time
import pandas as pd
import math
from pathlib import Path
import re
from telegram import send_telegram_notification
from pprint import pprint

address = ('localhost', 6000)


class Trade():
    """Execute new Trade"""

    def __init__(self, client, symbol, stop_loss, take_profit, quantity,
                 trde_start, on_going_trades, run_type, r2r, current_price,
                 precision):
        self.client = client
        self.bm = BinanceSocketManager(client)
        self.symbol = symbol
        self.stop_loss = stop_loss
        self.take_profit = take_profit
        self.quantity = quantity
        self.r2r = r2r
        self.current_price = current_price
        self.trde_start = trde_start
        self.run_type = run_type
        self.precision = precision
        self.interval = KLINE_INTERVAL_1MINUTE
        self.gain = 0
        self.initial_price = current_price
        self.upticks = 0
        self.downticks = 0
        self.trade_started = self.begin_trade(on_going_trades)

    def begin_trade(self, on_going_trades):
        try:
            buy_order = self.start_trade_order()
        except BinanceAPIException as e:
            print('Error:', e, file=fp)
            self.bm.close()
            if self.symbol in on_going_trades:
                on_going_trades.remove(self.symbol)
            return False
        ts = time.time()
        out = [
            self.symbol, self.current_price, self.stop_loss, self.take_profit,
            self.r2r, 'trade_started', self.trde_start,
            datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        ]
        with open('record.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerow(out)
        out = [str(x) for x in out]
        send_telegram_notification('-->'.join(out))
        self.start_price_streaming(on_going_trades)
        return True

    def start_trade_order(self):
        if self.run_type == 'test':
            start_trade_order = self.client.create_test_order(
                symbol=self.symbol,
                side=SIDE_BUY,
                type=ORDER_TYPE_MARKET,
                quantity=self.quantity)
        elif self.run_type == 'live':
            start_trade_order = self.client.create_order(
                symbol=self.symbol,
                side=SIDE_BUY,
                type=ORDER_TYPE_MARKET,
                quantity=self.quantity)
        print(self.symbol, ' ', "Buy order completed..", file=fp)
        return start_trade_order

    def stop_loss_order(self):
        if self.run_type == 'test':
            stop_loss_order = self.client.create_test_order(
                symbol=self.symbol,
                side=SIDE_SELL,
                type=ORDER_TYPE_MARKET,
                quantity=self.quantity)
        elif self.run_type == 'live':
            stop_loss_order = self.client.create_order(
                symbol=self.symbol,
                side=SIDE_SELL,
                type=ORDER_TYPE_MARKET,
                quantity=self.quantity)
        print(self.symbol, ' ', "Loss Order completed...", file=fp)
        return stop_loss_order

    def take_profit_order(self):
        if self.run_type == 'test':
            take_profit_order = self.client.create_test_order(
                symbol=self.symbol,
                side=SIDE_SELL,
                type=ORDER_TYPE_MARKET,
                quantity=self.quantity)
        elif self.run_type == 'live':
            take_profit_order = self.client.create_order(
                symbol=self.symbol,
                side=SIDE_SELL,
                type=ORDER_TYPE_MARKET,
                quantity=self.quantity)
        print(self.symbol, ' ', "Profit Order completed...", file=fp)
        return take_profit_order

    def start_price_streaming(self, on_going_trades):
        self.conn_key = self.bm.start_kline_socket(
            self.symbol,
            lambda msg,on_going_trades=on_going_trades: self.process_message(on_going_trades,msg),
            interval=self.interval)
        self.bm.start()
        print('streaming started', file=fp)

    def process_message(self, on_going_trades, msg):
        if msg['e'] == 'error':
            print('error caught in msg', file=fp)
            self.bm.stop_socket(self.conn_key)
            self.conn_key = self.bm.start_kline_socket(
                self.symbol, self.process_message, interval=KLINE_INTERVAL_1MINUTE)
            self.bm.start()

        base_asset_close_price = float(msg['k']['c'])
        print('on_going_trades:', on_going_trades, file=fp)

        if base_asset_close_price > self.current_price:
            self.upticks += 1
            gain = base_asset_close_price - self.current_price
            if gain > self.gain:
                self.stop_loss += gain
                self.stop_loss -= self.gain
                self.gain = gain

                debug = [
                    self.symbol, base_asset_close_price, self.stop_loss, self.take_profit
                ]
                print('changing trade params:', debug, file=fp)
                debug = [str(x) for x in debug]
                send_telegram_notification('-->'.join(debug))
        else:
            self.downticks += 1

        if base_asset_close_price >= self.take_profit or (
            self.upticks > 9 and (self.take_profit - base_asset_close_price) /
            (self.take_profit - self.current_price) < 0.5):
            try:
                profit_order = self.take_profit_order()
                success = 'profit' if self.initial_price < base_asset_close_price else 'loss'
                ts = time.time()
                out = [
                    self.symbol, self.initial_price, self.stop_loss,
                    base_asset_close_price, self.r2r, success, self.trde_start,
                    datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                ]
                with open('record.csv', 'a') as f:
                    writer = csv.writer(f)
                    writer.writerow(out)
                out = [str(x) for x in out]
                send_telegram_notification('-->'.join(out))

                if self.symbol in on_going_trades:
                    on_going_trades.remove(self.symbol)
                self.bm.stop_socket(self.conn_key)
                self.bm.close()
            except BinanceAPIException as e:
                print('Error:', e, file=fp)
                self.bm.stop_socket(self.conn_key)
                self.bm.close()

        elif (base_asset_close_price < self.stop_loss):
            try:
                loss_order = self.stop_loss_order()
                ts = time.time()
                if base_asset_close_price > self.initial_price:
                    success = 'profit'
                else:
                    success = 'loss'
                out = [
                    self.symbol, self.initial_price, base_asset_close_price,
                    self.take_profit, self.r2r, success, self.trde_start,
                    datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                ]
                with open('record.csv', 'a') as f:
                    writer = csv.writer(f)
                    writer.writerow(out)
                out = [str(x) for x in out]
                send_telegram_notification('-->'.join(out))
                if self.symbol in on_going_trades:
                    on_going_trades.remove(self.symbol)
                self.bm.stop_socket(self.conn_key)
                self.bm.close()
            except BinanceAPIException as e:
                print('Error:', e, file=fp)
                self.bm.close()


def execute_trade(client, symbol, stop_loss, take_profit, quantity,
                  on_going_trades, run_type, r2r, current_price, precision):
    ts = time.time()
    trde_start = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    new_trade = Trade(client, symbol, stop_loss, take_profit, quantity,
                      trde_start, on_going_trades, run_type, r2r, current_price,
                      precision)
    return new_trade


def rally_over(pct_change_in_prices, prices):
    # last_20_price = prices['closing'][-20:]
    # sum_last_20_price = sum(last_20_price)
    # intervals = [
    #     sum(last_20_price[0:5]) / sum_last_20_price,
    #     sum(last_20_price[5:10]) / sum_last_20_price,
    #     sum(last_20_price[10:15]) / sum_last_20_price,
    #     sum(last_20_price[15:20]) / sum_last_20_price
    # ]
    # if max(intervals) == intervals[3]:
    #     print('rally_over due to intervals', file=fp)
    #     return True
    if all([x > 1.0 for x in pct_change_in_prices[-5:]]):
        print('rally_over due to pct change', file=fp)
        return True
    if (prices['closing'][-1] -
        prices['closing'][-50]) / prices['closing'][-50] > 0.05:
        print('rally_over due to pct change in last 50 closing prices', file=fp)
        return True
    return False


def get_closing_prices(client, pair, frame, interval):
    prices = {'closing': [], 'opening': [], 'high': [], 'low': []}
    assert (frame * 50) / (60 * 24) <= interval, 'Need to extend interval'
    for kline in client.get_historical_klines_generator(
        pair, '{}m'.format(frame), "{} day ago UTC".format(interval)):
        prices['closing'].append(float(kline[4]))
        prices['high'].append(float(kline[2]))
        prices['opening'].append(float(kline[1]))
        prices['low'].append(float(kline[3]))
    return prices


def highs_lows(prices):
    last_50_closing_prices = prices['low'][-50:]
    price_series = pd.Series(last_50_closing_prices)
    pct_change_in_prices = price_series.pct_change()[1:]  # 1st element is nan
    highs = []
    lows = []
    for i, pct in enumerate(pct_change_in_prices):
        if abs(pct) < 0.01:  # rate of change --> 0
            if i + 1 < len(
                last_50_closing_prices
            ) and last_50_closing_prices[i +
                                         1] > last_50_closing_prices[i]:  # ensuring minima; not maxima
                lows.append(last_50_closing_prices[i])
            elif i + 1 < len(
                last_50_closing_prices
            ) and last_50_closing_prices[i +
                                         1] < last_50_closing_prices[i]:  # ensuring minima; not maxima
                highs.append(last_50_closing_prices[i])
    higher_highs_higher_lows = (all(highs[i] < highs[i + 1]
                                    for i in range(len(highs) - 1))
                                and all(lows[i] < lows[i + 1]
                                        for i in range(len(lows) - 1)))
    lower_highs_lower_lows = (all(highs[i] > highs[i + 1]
                                  for i in range(len(highs) - 1))
                              and all(lows[i] > lows[i + 1]
                                      for i in range(len(lows) - 1)))
    return higher_highs_higher_lows, lower_highs_lower_lows


def bearish_market(prices):
    last_3_closing_prices = prices['closing'][-3:]
    last_3_opening_prices = prices['opening'][-3:]
    last_3_high_prices = prices['high'][-3:]
    bearish_market = True
    for i, high in enumerate(last_3_high_prices):
        pivot = max(last_3_closing_prices[i], last_3_opening_prices[i])
        if (high - pivot) / high < 0.01:  #1% threshold
            bearish_market = False
    return bearish_market


def bullish_market(prices):
    last_3_closing_prices = prices['closing'][-3:]
    last_3_opening_prices = prices['opening'][-3:]
    last_3_low_prices = prices['low'][-3:]
    bullish_market = True
    for i, low in enumerate(last_3_low_prices):
        pivot = min(last_3_closing_prices[i], last_3_opening_prices[i])
        if (pivot - low) / pivot < 0.003:
            bullish_market = False
    return bullish_market


def get_stop_loss(last_50_low_prices):
    stop_loss_ind = None
    min_stop_loss = 99999.0
    price_series = pd.Series(last_50_low_prices)
    pct_change_in_prices = price_series.pct_change()[1:]  # 1st element is nan
    for i, pct in enumerate(pct_change_in_prices):
        if abs(pct) < 0.01:  # rate of change --> 0
            if i + 1 < len(
                price_series
            ) and price_series[i +
                               1] > price_series[i] and price_series[i] < min_stop_loss:  # ensuring minima; not maxima
                stop_loss_ind = i
                min_stop_loss = price_series[i]
    return stop_loss_ind


def get_take_profit(last_50_high_prices):
    take_profit_ind = None
    max_take_profit = 0.0
    price_series = pd.Series(last_50_high_prices)
    pct_change_in_prices = price_series.pct_change()[1:]  # 1st element is nan
    for i, pct in enumerate(pct_change_in_prices):
        if abs(pct) < 0.01:  # rate of change --> 0
            if i + 1 < len(
                price_series
            ) and price_series[i +
                               1] < price_series[i] and price_series[i] > max_take_profit:  # ensuring maxima; not minima
                take_profit_ind = i
                max_take_profit = price_series[i]
    return take_profit_ind


def hist_price_pattern_favorable(client, pair):
    ts = time.time()
    print(
        'Checking:',
        pair,
        'timestamp:',
        datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'),
        file=fp)

    # for interval in [30, 15, 5]:
    prices = get_closing_prices(client, pair, 5, 1)
    last_50_closing_prices = prices['closing'][-50:]
    price_series = pd.Series(last_50_closing_prices)
    pct_change_in_prices = price_series.pct_change()[1:]  # 1st element is nan
    current_price = last_50_closing_prices[-1]
    higher_highs_higher_lows, lower_highs_lower_lows = highs_lows(prices)

    if bearish_market(prices) or lower_highs_lower_lows:
        print('Bearish Market', file=fp)
        print('Exiting trade', file=fp)
        return False, None, None, None
    elif bullish_market(prices) or higher_highs_higher_lows:
        print('Bullish Market', file=fp)
        print('Taking trade', file=fp)
    elif rally_over(pct_change_in_prices, prices):
        print('rally may have come to an end in {}m chart'.format(5), file=fp)
        print('Exiting trade', file=fp)
        return False, None, None, None

    stop_loss_ind = get_stop_loss(prices['low'][-50:])
    take_profit_ind = get_take_profit(prices['high'][-50:])

    if stop_loss_ind:
        stop_loss = prices['low'][-50:][stop_loss_ind]
    else:
        print('suitable stop_loss could not be found', file=fp)
        print('Exiting trade', file=fp)
        return False, None, None, None

    if (current_price - stop_loss) <= 0.0:
        print('stop_loss:', stop_loss, file=fp)
        print('current_price:', current_price, file=fp)
        print("Stop Loss greater than or equal to current_price", file=fp)
        print("Bottom not found.", file=fp)
        print("Exiting trade", file=fp)
        return False, None, None, None
    percentage_risk = (current_price - stop_loss) / current_price
    if percentage_risk > 0.1:
        take_profit = current_price + (
            (1 + percentage_risk) * (current_price - stop_loss))
        multiplier = (1 + percentage_risk)
    else:
        multiplier = 1.5
        take_profit = current_price + multiplier * (current_price - stop_loss)
    print('initial take profit:', take_profit, file=fp)
    print('initial stop_loss:', stop_loss, file=fp)

    probable_take_profit = 0.0
    if take_profit_ind:
        probable_take_profit = prices['high'][-50:][take_profit_ind]
    print('probable_take_profit:', probable_take_profit, file=fp)

    if (probable_take_profit > current_price
        and probable_take_profit < take_profit):
        take_profit = probable_take_profit
        stop_loss = current_price - ((take_profit - current_price) / multiplier)
    if (current_price - take_profit) >= 0.0:
        print('take_profit:', take_profit, file=fp)
        print('current_price:', current_price, file=fp)
        print("Take Profit smaller than or equal to current_price", file=fp)
        print('Exiting trade', file=fp)
        return False, None, None, None
    if (current_price - stop_loss) <= 0.0:
        print('stop_loss:', stop_loss, file=fp)
        print('current_price:', current_price, file=fp)
        print("Stop Loss greater than or equal to current_price", file=fp)
        print("Exiting trade", file=fp)
        return False, None, None, None
    print('final take_profit:', take_profit, file=fp)
    print('final stop_loss:', stop_loss, file=fp)
    print('final current_price:', current_price, file=fp)
    return True, stop_loss, take_profit, current_price


def trim_by_step_size(num_units, step_size):
    pre, post = step_size.split('.')
    position = 1
    unit_precision = True  # 1.000000
    for ch in post:
        if ch == '0':
            position += 1
        elif ch == '1':
            unit_precision = False
            break

    if unit_precision:
        return int(float(num_units))
    else:
        pre, post = num_units.split('.')
        new_post = ""
        for ch in post:
            if position >= 1:
                new_post += ch
                position -= 1
            else:
                new_post += '0'
        return '.'.join([pre, new_post])


def get_quantity(client,
                 pair,
                 stop_loss,
                 take_profit,
                 current_price,
                 num_allowed_trades,
                 investment=0.0):
    investment_percentage = 0.001
    recalculation_required = False
    while True:
        risk = investment_percentage * investment
        unit_risk = current_price - stop_loss
        unit_reward = take_profit - current_price
        reward_to_risk = unit_reward / unit_risk
        num_units = float(risk / unit_risk)
        if recalculation_required:
            recalculation_required = False
        if num_units * current_price > investment / num_allowed_trades:
            print('Low BTC balance for this order', file=fp)
            return 0.0, reward_to_risk, 0.0
        exchange_info = client.get_exchange_info()
        for symbol_dict in exchange_info['symbols']:
            if symbol_dict['symbol'] == pair:
                filters = symbol_dict['filters']
                for filter_dict in filters:
                    if filter_dict['filterType'] == 'LOT_SIZE':
                        if not (num_units >= float(filter_dict['minQty'])
                                and num_units <= float(filter_dict['maxQty'])):
                            print('Malformed LOT_SIZE:', num_units, filter_dict['minQty'], file=fp)
                            return 0.0, reward_to_risk, 0.0
                        else:
                            precision = symbol_dict['baseAssetPrecision']
                            step_size = str(filter_dict['stepSize'])
                    elif filter_dict['filterType'] == 'MIN_NOTIONAL' and filter_dict['applyToMarket']:
                        if num_units * current_price <= float(filter_dict['minNotional']):
                            print(
                                'Malformed MIN_NOTIONAL:',
                                num_units * current_price,
                                filter_dict['minNotional'],
                                file=fp)
                            investment_percentage = 2 * investment_percentage
                            recalculation_required = True
        if not recalculation_required:
            break

    num_units = "{:0.0{}f}".format(num_units, precision)
    num_units = trim_by_step_size(num_units, step_size)
    return float(num_units), reward_to_risk, precision


def has_high_win_percentage(pair):
    losses = 0
    profits = 0
    for line in open('/home/ankesh/traderbot/record.csv', 'r'):
        if re.search(pair, line):
            if re.search('loss', line):
                losses += 1
            elif re.search('profit', line):
                profits += 1
    return profits >= losses


def get_high_volume_pairs(all_values):
    eligible_pairs = defaultdict(int)
    for interval, values in all_values.items():
        for i, pair in enumerate(values['Base Asset(1)/Quote Asset(2)']):
            if (values['percentage change in price (1)'][i] > -0.9
                and values['percentage change in volume wrt 24h volume of (2)'][i] >
                0.05 * int(interval.rstrip('m'))
                and (values['Moving average(7) > Moving average(25)'][i]
                     or values['percentage change in price (1)'][i] > -0.1)
                and has_high_win_percentage(pair)):
                eligible_pairs[pair] += 1
    pprint(eligible_pairs)
    return [pair for pair, val in eligible_pairs.items() if val > 1]


def main(all_values, client, on_going_trades, trades_taken, run_type='test'):
    traded = False
    balance = float(client.get_asset_balance(asset='BTC')['free'])
    num_allowed_trades = 2

    high_volume_pairs = get_high_volume_pairs(all_values)
    print('high_volume_pairs:', high_volume_pairs)
    if len(trades_taken) > 9:
        print('Already 10 trades in progress..', file=fp)
        return None
    for pair in high_volume_pairs:
        if pair not in on_going_trades:
            (is_favorale_trade, stop_loss, take_profit,
             current_price) = hist_price_pattern_favorable(client, pair)
            if is_favorale_trade:
                quantity, r2r, precision = get_quantity(
                    client,
                    pair,
                    stop_loss,
                    take_profit,
                    current_price,
                    num_allowed_trades,
                    investment=balance,
                )
                if quantity > 0.0:
                    on_going_trades.append(pair)
                    trades_taken.append(pair)
                    _ = execute_trade(client, pair, stop_loss, take_profit, quantity,
                                      on_going_trades, run_type, r2r, current_price,
                                      precision)
                    traded = True
            print('#' * 50, file=fp)
    if not traded:
        print('Suitable trades not found...')


# def sigint_handler(signal, frame):
#     print('',file=fp)
#     print('KeyboardInterrupt received. Shutting down...',file=fp)
#     client = Client(os.environ['api_key'], os.environ['api_secret'])
#     for pair in on_going_trades:
#         balance = float(
#             client.get_asset_balance(asset=pair.replace('BTC', ''))['free'])
#         if sys.argv[1] == 'live':
#             stop_loss_order = client.create_order(
#                 symbol=pair, side=SIDE_SELL, type=ORDER_TYPE_MARKET, quantity=balance)
#         else:
#             stop_loss_order = client.create_test_order(
#                 symbol=pair, side=SIDE_SELL, type=ORDER_TYPE_MARKET, quantity=balance)
#     sys.exit(0)


def server(on_going_trades):
    listener = Listener(address, authkey=b'secret password')
    client = Client(os.environ['api_key'], os.environ['api_secret'])

    trades_taken = []
    with open('record.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow([
            'symbol', 'current_price', 'stop_loss', 'take_profit', 'reward/risk',
            'result', 'trade_start_at', 'trade_completed_at'
        ])
    if sys.argv[1] == 'test':
        send_telegram_notification('Running Tests.............................')
    elif sys.argv[1] == 'live':
        send_telegram_notification('Running live trades. Monitor at binance...')
    while True:
        try:
            with open('/home/ankesh/traderbot/temp.json', 'r') as f:
                all_values = json.load(f)
            if sys.argv[1] == 'test':
                main(all_values, client, on_going_trades, trades_taken, 'test')
            elif sys.argv[1] == 'live':
                main(all_values, client, on_going_trades, trades_taken, 'live')
        except Exception as e:
            print('Error:', e, file=fp)
        time.sleep(1)
    # signal.signal(signal.SIGINT, sigint_handler)


if __name__ == '__main__':
    on_going_trades = []
    fp = open('trades.log', 'w')
    server(on_going_trades)