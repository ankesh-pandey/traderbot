from binance.client import Client
from pprint import pprint
from env import all_values
from prettytable import PrettyTable
import time
import json
import datetime
from binance.websockets import BinanceSocketManager


class Web_socket():
	"""web_socket"""

	def __init__(self, client, symbol, interval, curr_values, _24h_btc_volume):
		self.client = client
		self.symbol = symbol
		self.interval = interval
		self.curr_values = curr_values
		self._24h_btc_volume = _24h_btc_volume
		self.bm = BinanceSocketManager(client)
		self.conn_key = self.bm.start_kline_socket(
		    self.symbol, self.process_message, interval=self.interval)

	def run_forever(self):
		self.bm.start()

	def destroy(self):
		self.bm.stop_socket(self.conn_key)
		self.bm.close()

	def process_message(self, msg):
		global all_values
		if msg['e'] == 'error':
			self.bm.stop_socket(self.conn_key)
			self.conn_key = self.bm.start_kline_socket(
			    self.symbol, self.process_message, interval=self.interval)
			self.bm.start()
		base_asset_volume = msg['k']['v']
		quote_asset_volume = msg['k']['q']
		base_asset_close_price = msg['k']['c']

		self.curr_values[self.symbol[:3]] = base_asset_volume
		if float(quote_asset_volume) >= float(self.curr_values[self.symbol[3:]]):
			self.curr_values[self.symbol[3:]] = quote_asset_volume
		else:
			# interval completed completed
			now = time.time()
			hist_quote_volumes = []
			base_assest_closing_prices = []
			interval_to_history = {
			    '1m': "1 day ago UTC",
			    '5m': "1 day ago UTC",
			    '15m': "1 day ago UTC",
			    "30m": "2 day ago UTC",
			    "1h": "3 day ago UTC",
			    "4h": "10 day ago UTC"
			}
			for kline in self.client.get_historical_klines_generator(
			    self.symbol, self.interval, interval_to_history[self.interval]):
				hist_quote_volumes.append(kline[7])
				base_assest_closing_prices.append(kline[4])

			hist_quote_volumes = hist_quote_volumes[:-2]
			base_assest_closing_prices = base_assest_closing_prices[:-1]
			hist_moving_avg = sum([float(x) for x in hist_quote_volumes[-20:]]) / 20.0
			base_asset_moving_avg = sum(
			    [float(x) for x in base_assest_closing_prices[-20:]]) / 20.0
			self.sma = base_asset_moving_avg
			last_1m_volume = float(self.curr_values[self.symbol[3:]])
			percentage_change_in_price = (
			    float(base_asset_close_price) -
			    base_asset_moving_avg) * 100 / base_asset_moving_avg
			percentage_change_in_volume = (
			    last_1m_volume - hist_moving_avg) * 100 / hist_moving_avg

			# update values dict
			self.curr_values[self.symbol[3:]] = quote_asset_volume

			listed = [
			    'Base Asset(1)/Quote Asset(2)', 'price moving average of (1)',
			    'latest price of (1)', 'percentage change in price (1)',
			    'moving average of volume (2)', 'latest volume (2)',
			    'percentage change in volume (2)',
			    'percentage change in volume wrt 24h volume of (2)',
			    'Moving average(7) > Moving average(25)'
			]
			moving_avg_7 = sum([float(x)
			                    for x in base_assest_closing_prices[-7:]]) / 7.0
			moving_avg_25 = sum([float(x)
			                     for x in base_assest_closing_prices[-25:]]) / 25.0
			percent_change_wrt_24h_volume = last_1m_volume * 100 / self._24h_btc_volume
			listed_vals = [
			    self.symbol,
			    float(base_asset_moving_avg),
			    float(base_asset_close_price),
			    float(percentage_change_in_price),
			    float(hist_moving_avg),
			    float(last_1m_volume),
			    float(percentage_change_in_volume),
			    float(percent_change_wrt_24h_volume),
			    float(moving_avg_25) < float(moving_avg_7)
			]

			try:
				ind = all_values[self.interval]['Base Asset(1)/Quote Asset(2)'].index(
				    self.symbol)
				for i, item in enumerate(listed):
					all_values[self.interval][item][ind] = listed_vals[i]
			except ValueError:
				for i, item in enumerate(listed):
					all_values[self.interval][item].append(listed_vals[i])
			#sorting
			for item in listed:
				if item in all_values[self.interval] and all_values[self.interval][item]:
					all_values[self.interval][item] = [
					    x for _, x in sorted(
					        zip(
					            all_values[self.interval][
					                'percentage change in volume wrt 24h volume of (2)'],
					            all_values[self.interval][item]),
					        reverse=True)
					]
			with open('temp.json', 'w') as f:
				json.dump(all_values, f)
			ts = time.time()
			time_stamp = datetime.datetime.fromtimestamp(ts).strftime(
			    '%Y-%m-%d %H:%M:%S')
			print(self.symbol, ' ', self.interval, ' results updated in :',
			      time.time() - now, ' seconds;', 'Timestamp:', time_stamp)
