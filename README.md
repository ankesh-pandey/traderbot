# binanceApp

## Install
```
sudo apt-get install python3 python3-dev virtualenv
```
```
virtualenv -p python3 binanceApp
```
```
source binanceApp/bin/activate
```
```
pip install twisted[tls]
```
```
pip install -r requirements.txt
```

## Run
To stream live candlesticks
```
python get_data_stream.py
```

To run trading engine
```
python run_trading_engine.py
```

To start accepting and executing trades
```
python test.py
```


