import os
import env
import time
import signal
import sys
from pprint import pprint

from binance.client import Client
from twisted.internet import reactor
from binance.enums import *
from binance.exceptions import BinanceAPIException

from web_socket import Web_socket


def sigint_handler(signal, frame):
	print('')
	print('KeyboardInterrupt received. Shutting down...')
	close_all(connections)
	sys.exit(0)


def close_all(connections):
	for connection in connections:
		connection.destroy()
	reactor.stop()


def main(connections):
	client = Client(os.environ['api_key'], os.environ['api_secret'])
	# get system status
	status = client.get_system_status()
	assert status['msg'] == 'normal', (
	    "System status not normal. Do not proceed!")

	# get exchange info
	# this has rateLimits, server time and symbols info
	exchange_info = client.get_exchange_info()

	trading_symbols = []
	for symbol_dict in exchange_info['symbols']:
		if symbol_dict['status'] == 'TRADING':
			trading_symbols.append(symbol_dict['symbol'])

	# candlestick streaming
	intervals = [
	    KLINE_INTERVAL_1MINUTE,
	    KLINE_INTERVAL_5MINUTE,
	    KLINE_INTERVAL_15MINUTE,
	    KLINE_INTERVAL_30MINUTE,
	    #KLINE_INTERVAL_1HOUR,
	    #KLINE_INTERVAL_4HOUR,
	    #KLINE_INTERVAL_1DAY,
	    #KLINE_INTERVAL_1WEEK
	]
	for interval in intervals:
		print("Streaming {} live candlesticks...".format(interval))
		for symbol in trading_symbols:
			if symbol[-3:] == 'BTC':
				info = client.get_ticker(symbol=symbol)
				_24h_btc_volume = float(info['quoteVolume'])
				if _24h_btc_volume > 500.0:
					curr_values = {symbol[:3]: 0.0, symbol[3:]: 0.0}
					connection = Web_socket(client, symbol, interval, curr_values,
					                        _24h_btc_volume)
					connection.run_forever()
					connections.append(connection)
					time.sleep(10)
		print("{} connections made".format(len(connections)))
		print(interval, ' connections completed...')
	signal.signal(signal.SIGINT, sigint_handler)


if __name__ == '__main__':
	connections = []
	try:
		main(connections)
	except BinanceAPIException as e:
		print(e.status_code)
		print(e.message)
		close_all(connections)
		sys.exit(0)
