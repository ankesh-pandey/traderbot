import os
import env
import sys
from binance.client import Client
from binance.enums import *
from binance.exceptions import BinanceAPIException
from binance.websockets import BinanceSocketManager
from telegram import send_telegram_notification
from twisted.internet import reactor
from pprint import pprint
import time


def start_trade_order(symbol, quantity, run_type):
	print('starting trade for ', symbol, '; quantity:', quantity, file=fp)
	if run_type == 'test':
		start_trade_order = client.create_test_order(
		    symbol=symbol, side=SIDE_BUY, type=ORDER_TYPE_MARKET, quantity=quantity)
	elif run_type == 'live':
		start_trade_order = client.create_order(
		    symbol=symbol, side=SIDE_BUY, type=ORDER_TYPE_MARKET, quantity=quantity)
	print(symbol, ' ', "Buy order completed..", file=fp)
	return start_trade_order


def take_profit_order(symbol, quantity, run_type):
	print('closing trade for ', symbol, '; quantity:', quantity, file=fp)
	if run_type == 'test':
		take_profit_order = client.create_test_order(
		    symbol=symbol, side=SIDE_SELL, type=ORDER_TYPE_MARKET, quantity=quantity)
	elif run_type == 'live':
		take_profit_order = client.create_order(
		    symbol=symbol, side=SIDE_SELL, type=ORDER_TYPE_MARKET, quantity=quantity)
	print(symbol, ' ', "Sell Order completed...", file=fp)
	return take_profit_order


def trim_by_step_size(num_units, step_size):
	pre, post = step_size.split('.')
	position = 1
	unit_precision = True  # 1.000000
	for ch in post:
		if ch == '0':
			position += 1
		elif ch == '1':
			unit_precision = False
			break

	if unit_precision:
		return int(float(num_units))
	else:
		pre, post = num_units.split('.')
		new_post = ""
		for ch in post:
			if position >= 1:
				new_post += ch
				position -= 1
			else:
				new_post += '0'
		return '.'.join([pre, new_post])


def get_quantity(price, symbol):
	ten_prcnt_balance = float(client.get_asset_balance(asset='BTC')['free']) * 0.1
	quantity = ten_prcnt_balance / price
	quantity = "{:0.0{}f}".format(quantity, precision[symbol])
	quantity = trim_by_step_size(str(quantity), step_size[symbol])
	return float(quantity)


def rally_over(first, last, symbol):
	if ((last - first) / first > 0.02):
		print(symbol, ' rally_over', file=fp)
		return True
	return False


def process_message(bm, stats, symbol, msg):
	try:
		now = time.time()
		stats['temp_volume'] += float(msg['k']['q'])
		stats['temp_num_trades'] += int(msg['k']['n'])
		if msg['k']['x']:
			print('updating kline...', file=fp)
			print('prices:', stats['prices'], file=fp)
			print('volumes:', stats['volumes'], file=fp)
			print('avg_price:', stats['avg_price'], file=fp)
			print('avg_volume:', stats['avg_volume'], file=fp)
		else:
			if symbol in trading and (
			    float(msg['k']['c']) >= start_price[symbol] * 1.005
			    or float(msg['k']['c']) <= start_price[symbol] * 0.995):
				send_telegram_notification(symbol + ' Selling price:' + str(msg['k']['c']))
				_ = take_profit_order(symbol, quantities[symbol], trade_type)
				trading.remove(symbol)
				confirmations[symbol] = 0
			elif float(msg['k']['c']) >= start_price[symbol] * 1.0025:
				start_price[symbol] = float(msg['k']['c'])
				print(symbol, ' start prince updated to ', float(msg['k']['c']))
			return None

		#percent changes
		if len(stats['prices']) == 7:
			prcnt_chg_in_price = (
			    float(msg['k']['c']) - stats['avg_price']) * 100 / stats['avg_price']
			prcnt_chg_in_volume = (
			    stats['temp_volume'] - stats['avg_volume']) * 100 / stats['avg_volume']
			prcnt_chg_in_volume_24h = (
			    stats['temp_volume']) * 100 / _24h_btc_volumes[symbol]
			print(
			    symbol,
			    'percent_change_in_price:',
			    str.format('{0:.2f}', prcnt_chg_in_price),
			    file=fp)
			print(
			    symbol,
			    'percent_change_in_volume:',
			    str.format('{0:.2f}', prcnt_chg_in_volume),
			    file=fp)
			print(
			    symbol,
			    'percent_change_in_volume_24h:',
			    str.format('{0:.2f}', prcnt_chg_in_volume_24h),
			    file=fp)

			#trading opportunity
			if float(msg['k']['c']) > stats['avg_price'] and float(
			    msg['k']['c']) > stats['prices'][-1] and not rally_over(
			        stats['prices'][0], float(msg['k']['c']),
			        symbol) and symbol not in trading:
				confirmations[symbol] += 1
				print(symbol, 'confirmation incremented:', confirmations[symbol])
			else:
				print(symbol, ' grter than avg_price: ',
				      float(msg['k']['c']) > stats['avg_price'], 'grter than last price:',
				      float(msg['k']['c']) > stats['prices'][-1], 'not rally_over:',
				      not rally_over(stats['prices'], symbol))
				confirmations[symbol] = 0

			if (confirmations[symbol] > 2 or
			    (confirmations[symbol] > 1
			     and prcnt_chg_in_volume >= 100)) and symbol not in trading:
				send_telegram_notification(symbol + ' Buying price:' + str(msg['k']['c']))
				quantities[symbol] = get_quantity(float(msg['k']['c']), symbol)
				start_price[symbol] = float(msg['k']['c'])
				_ = start_trade_order(symbol, quantities[symbol], trade_type)
				trading.append(symbol)
				confirmations[symbol] = 0

			if symbol in trading and (
			    float(msg['k']['c']) < stats['avg_price']
			    or float(msg['k']['c']) >= start_price[symbol] * 1.005
			    or float(msg['k']['c']) <= start_price[symbol] * 0.995):
				send_telegram_notification(symbol + ' Selling price:' + str(msg['k']['c']))
				_ = take_profit_order(symbol, quantities[symbol], trade_type)
				trading.remove(symbol)
				confirmations[symbol] = 0

		#update averages
		if stats['avg_price'] is None:
			stats['avg_price'] = float(msg['k']['c'])
		else:
			stats['avg_price'] = (stats['avg_price'] * len(stats['prices']) + float(
			    msg['k']['c'])) / (len(stats['prices']) + 1)

		if stats['avg_volume'] is None:
			stats['avg_volume'] = stats['temp_volume']
		else:
			stats['avg_volume'] = (stats['avg_volume'] * len(stats['volumes']) +
			                       stats['temp_volume']) / (len(stats['volumes']) + 1)

		if stats['avg_num_trades'] is None:
			stats['avg_num_trades'] = stats['temp_num_trades']
		else:
			stats['avg_num_trades'] = (
			    stats['avg_num_trades'] * len(stats['num_trades']) +
			    stats['temp_num_trades']) / (len(stats['num_trades']) + 1)

		#update lists
		stats['prices'].append(float(msg['k']['c']))
		stats['volumes'].append(stats['temp_volume'])
		stats['num_trades'].append(stats['temp_num_trades'])

		#reset temp values for new kline
		stats['temp_num_trades'] = 0
		stats['temp_volume'] = 0.0

		#truncate lists
		if len(stats['prices']) >= 8:
			stats['prices'].pop(0)
		if len(stats['volumes']) >= 8:
			stats['volumes'].pop(0)
		if len(stats['num_trades']) >= 8:
			stats['num_trades'].pop(0)

		print(symbol, ' time taken in calculations:', time.time() - now, file=fp)
		print('-' * 30, file=fp)
	except Exception as e:
		print('Error:', str(e), file=fp)


def main():
	# create client

	# get system status
	status = client.get_system_status()
	assert status['msg'] == 'normal', (
	    "System status not normal. Do not proceed!")

	#get exchange info
	exchange_info = client.get_exchange_info()

	trading_symbols = []
	for symbol_dict in exchange_info['symbols']:
		if symbol_dict['status'] == 'TRADING' and symbol_dict['symbol'][-3:] == 'BTC':
			trading_symbols.append(symbol_dict['symbol'])
			filters = symbol_dict['filters']
			for filter_dict in filters:
				if filter_dict['filterType'] == 'LOT_SIZE':
					precision[symbol_dict['symbol']] = symbol_dict['baseAssetPrecision']
					step_size[symbol_dict['symbol']] = str(filter_dict['stepSize'])

	# start price streaming
	stats = {}
	for symbol in trading_symbols:
		info = client.get_ticker(symbol=symbol)
		_24h_btc_volume = float(info['quoteVolume'])
		if _24h_btc_volume < 1500.0:
			continue
		_24h_btc_volumes[symbol] = _24h_btc_volume
		confirmations[symbol] = 0
		stats[symbol] = {
		    'prices': [],
		    'volumes': [],
		    'num_trades': [],
		    'avg_price': None,
		    'avg_volume': None,
		    'avg_num_trades': None,
		    'temp_volume': 0.0,
		    'temp_num_trades': 0
		}
		bm = BinanceSocketManager(client)
		conn_key = bm.start_kline_socket(
		    symbol,
		    lambda msg, bm=bm, stats=stats[symbol],symbol=symbol: process_message(bm, stats,symbol,msg),
		    interval=KLINE_INTERVAL_1MINUTE)
		bm.start()
		time.sleep(10)


if __name__ == "__main__":
	try:
		fp = open('trades.log', 'w')
		send_telegram_notification('Started streaming....')
		quantities = {}
		trading = []
		step_size = {}
		precision = {}
		start_price = {}
		confirmations = {}
		_24h_btc_volumes = {}
		trade_type = str(sys.argv[1])
		client = Client(os.environ['api_key'], os.environ['api_secret'])
		main()
	except Exception as e:
		print('Error code in main:', e.status_code, file=fp)
		print('Error msg in main:', e.message, file=fp)
		print('Error:', str(e), file=fp)